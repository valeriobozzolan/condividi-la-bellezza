$(document).ready(function() {


});

$(window).on("load", function() {

  grayboxHeight();
  changeField();
  imageHeight();

  $(".sendform").click(function() {
    var current_form = $(this).parents('form');
    var isFormValid = checkField(current_form);
    if (isFormValid === true) {
      // mettici dentro tutto
      var formitems = [];

      var posturl = 'response.php';

      var form = $(this).parents('form');

      $(form).find('input:not([type="checkbox"]), textarea, select').each(function(i) {
        formitem = {}
        formitem.name = $(this).attr('name');
        formitem.value = $(this).val();
        formitems.push(formitem);
        //console.log(name+' '+value);
      });

      $(form).find('input[type="checkbox"]').each(function(i) {
        formitem = {}
        formitem.name = $(this).attr('name');
        if ($(this).is(':checked')) {
          formitem.value = 'ok';
        } else {
          formitem.value = 'no';
        }
        formitems.push(formitem);
      });


      // console.log(formitems);

      $.post(posturl, formitems, function(result) {
        // console.log(result);
        var ajaxresult = $(form).parents('.modal-body').find('.ajaxresult');
        $(form).hide();
        //$(ajaxresult).addClass('mario');
        $(ajaxresult).html(result);
        $(ajaxresult).fadeIn(500, "linear");
      });
    } else {
      return false;
    }
  });
});

$(window).resize(function() {
  grayboxHeight();
  imageHeight();
}); // fine window resize function

function grayboxHeight() {
  var max_height = 0;
  $('#do-you-know .graybox').each(function() {
    var currentHeight = $(this).height();
    if (currentHeight > max_height) {
      max_height = currentHeight;
    }
  });
  $('#do-you-know .graybox').height(max_height);
}

function imageHeight(){

  imgRatio = 1.5;
  imgW = $('#cards-grid .col-12').width();
  imgH = imgW/imgRatio;

  $('#cards-grid .col-12 .card_img').height(imgH);

}

function checkField(current_form) {
  //var current_form = $(this).parents('form');
  $(current_form).addClass('is_the_current_form');
  var re = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  if (
    ($(current_form).find('input[type=text]').val().length > 2) &&
    (re.test(($(current_form).find("input[type=email]").val()))) &&
    ($(current_form).find("input[type=checkbox]").is(':checked'))
  ) {
//    console.log('form valid');
    $(current_form).find('.sendform').addClass('enabled');
    return true;
  } else {
//    console.error('form NOT valid');
    $(current_form).find('.sendform').removeClass('enabled');
    return false;
  }
}

function sendmailajax() {

}

function changeField() {
  $('.modal-content input').on('change', function() {
    var current_form = $(this).parents('form');
    var pippo = checkField(current_form);

    if (pippo === true) {

    } else {
      pippo === false;
    }
  })
}
