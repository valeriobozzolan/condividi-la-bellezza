<?php
$email_html='


<html>
  <body style="text-align: center; background-color:#eaf0f6;">

      <table width="550px" border="0" cellspacing="0" cellpadding="0" style="text-align: left; font-family: Arial, Helvetica, Sans Serif; font-size: 16px; margin-left:auto; margin-right:auto; background-color: #ffffff; border: 15px solid #ffffff;">
        <tr>
            <td>
                <img src="'.$baseurl.'/img/mail_header.jpg" alt="Wikimedia Italia" style="width: 550px; height: auto;" width="550" height="auto"/>
            </td>
        </tr>

        <tr>
            <td style="padding: 15px 0px;">
              <img src="'.$baseurl.'/img/mail_separator_spaces.png" alt="Wikimedia Italia" style="width: 550px; height: 25px;" width="550" height="25"/>
            </td>
        </tr>

        <tr>
          <td style="padding: 15px 0px;">
            <h1>Hai ricevuto un regalo!</h1>
            <p><strong>Ciao '.$dest_name.'!</strong></p>
            <p>'.$mit_name.' ti ha donato una cartolina!</p>
            <br/>
            <table width="550" border="15" cellspacing="0" cellpadding="0" bgcolor="#11689c" style="width: 550px; border: 15px solid #11689c; background: #11689c; color: #fff; text-align: center; font-family: Arial, Helvetica, Sans Serif; font-size: 14px; border-collapse: collapse;">
              <tr>
                <td>
                  <div>'.$mit_msg.'</div>
                </td>
              </tr>
            </table>

          </td>
        </tr>
        <tr>
            <td style="padding: 15px 0px;">
              <img src="'.$card_teaser.'" alt="'.$card_title.'" width="550" height="auto" style="width: 550px; height: auto;" />
            </td>
        </tr>
        <tr>
            <h3>'.$card_title.'</h3>
            <p>'.$card_subject.' - '.$card_where.'</p>
            <p><a href="'.$card_full.'">Scarica la versione in alta risoluzione</a></p>
            <p>Foto di '.$card_auth_name.'<br/><em>'.$card_licence.'</em></p>
        </tr>
        <tr>
            <td style="padding: 15px 0px;">
              <img src="'.$baseurl.'/img/mail_separator_spaces.png" alt="Wikimedia Italia" style="width: 550px; height: 25px;" width="550" height="25""/>
            </td>
        </tr>
        <tr>
          <td>
            <img src="'.$baseurl.'/img/wmi-footer.png" alt="Wikimedia Italia">
            <p><strong>WIKIMEDIA ITALIA</strong><br>
              <i>associazione per la diffusione della conoscenza libera</i></p>
            <p></p>
            <p style="font-size: 12px;"><i>Via Bergognone, 34 – 20144 Milano (MI)</i></p>
            <p style="font-size: 12px;"><i>Tel. (+39) 02 97677170 | Fax. (+39) 02 56561506 | <a href="mailto:segreteria@wikimedia.it">segreteria@wikimedia.it</a> | <a href="mailto:wikimediaitalia@pec.it">wikimediaitalia@pec.it</a></i></p>
              <p style="font-size: 12px;"><i>P.Iva 05599740965 | CF 94039910156 | Codice SDI KRRH6B9</i></p>
          </td>
        </tr>
      </table>

</body>

</html>

';
