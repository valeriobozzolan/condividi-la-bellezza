<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $site_description; ?>">
    <meta name="author" content="Stefano Cannillo | Emerald Communication">
    <meta name="generator" content="Custom Page">


    <!-- Matomo Tag Manager -->
    <script>
    var _mtm = window._mtm = window._mtm || [];
    _mtm.push({'mtm.startTime': (new Date().getTime()), 'event': 'mtm.Start'});
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src='https://matomo.wikimedia.it/js/container_ewjHFlfk.js'; s.parentNode.insertBefore(g,s);
    </script>
    <!-- End Matomo Tag Manager -->


    <title><?php echo $site_name; ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/wmi-favicon.ico" />
    <link rel="icon" type="image/x-icon"  href="/wmi-favicon.ico">

    <script defer src="/theme/all.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/theme/bootstrap.min.css">
    <link rel="stylesheet" href="/theme/style.css" >

    <meta name="theme-color" content="#7952b3">

    <!-- OG tags -->
    <meta property="og:title" content="<?php echo $site_name; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://condividilabellezza.wikimedia.it" />
    <meta property="og:image" content="/img/teaser_mareggiata_a_polignano.jpg">
    <meta property="og:description" content="<?php echo $site_description; ?>">

  </head>

  <body>

    <div id="testata">

      <div id="testata_img">
        <picture>
          <source media="(min-width:991px)" srcset="<?php echo $baseurl; ?>img/card_header_desktop.jpg">
          <source media="(min-width:760px)" srcset="<?php echo $baseurl; ?>img/card_header_tablet.jpg">
          <img src="<?php echo $baseurl; ?>img/card_header_mobile.jpg" alt="Wikimedia Italia">
        </picture>
        <div id="header_cover"></div>
      </div>
      <div id="testata_content">
          <div class="logo-wrapper">
            <div class="logo">
              <img alt="Wikimedia Italia Logo" src="<?php echo $baseurl; ?>img/vert_white_wikimedia.png">
              <img alt="Wikimedia Italia Logo" src="<?php echo $baseurl; ?>img/white_wlm.png">
            </div>
          </div>
          <h1>
            CONDIVIDI LA BELLEZZA,<br>DIFFONDI LA CONOSCENZA LIBERA
          </h1>

          <div class="header-button"><a id="donaora_button_top" class="bg_red" href="#cards-grid">
            INVIA UNA CARTOLINA<br/>A CHI AMI</a>
          </div>
        </div>
      </div>
