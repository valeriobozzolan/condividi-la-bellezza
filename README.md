# Condividi la bellezza

Questo è il codice sorgente del sito "Condividi la Bellezza", un sito legato alla campagna di fundraising di Wikimedia Italia 2021-2022, sviluppato nel 2022 da Emerald Communication.

https://condividilabellezza.wikimedia.it 

Informazioni sul progetto:

https://wiki.wikimedia.it/wiki/Siti/Condividi_la_bellezza_2022

## Configurazione

Copiare il file `config-private-example.php` in `config-private.php` e popolare quest'ultimo.

## License

Copyright (C) 2022 [Emerald Communication](https://emeraldcommunication.com/)

Copyright (C) 2022 rispettivi contributori

Questo programma è software libero: puoi ridistribuirlo e/o modificarlo
seguendo i termini della GNU General Public License pubblicata
dalla Free Software Foundation, sia la versione 3 della Licenza, che
(a tua scelta) una qualsiasi versione successiva.
Questo programma è distribuito nella speranza che possa essere utile,
ma viene fornito SENZA NESSUNA GARANZIA.
Vedi la GNU General Public License per ulteriori dettagli.
Dovresti aver ricevuto una copia della GNU General Public License
insieme a questo programma. In caso contrario, vedi https://www.gnu.org/licenses/.
