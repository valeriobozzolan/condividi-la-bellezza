<?php
  require 'config.php';
  require 'libs/functions.php';
  include 'theme/header.php';
?>

<div id='wrapper'>

  <div class="green-box">
    <div class="container text-center">
      <p>Regala uno scorcio dell’immenso patrimonio artistico italiano a chi ami. Scegli una delle foto vincitrici del concorso Wiki Loves Monuments e condividi la bellezza!</p>
    </div>
  </div>
  <div class="green-triangle-wrapper">
    <div class="green-triangle">

    </div>
  </div>

  <div id="what-is" class="text-center">
    <div class="inner">
      <div class="intro">
        <h3>BASTANO 3 SEMPLICI PASSI</h3>
      </div>
      <div class="box-wrapper container">
        <div class="row">
          <div class="col-12 col-md-4">
            <div class="box-container">
              <div class="graybox">
                <div class="icon">
                  <img src="/img/scegli.svg" alt="Wikimedia Italia">
                </div>
                <div class="description">
                  <h4>SCEGLI</h4>
                  <p>una foto tra le vincitrici<br>di Wiki Loves Monuments Italia</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4">
            <div class="box-container">
              <div class="graybox">
                <div class="icon">
                  <img src="/img/manda.svg" alt="Wikimedia Italia">
                </div>
                <div class="description">
                  <h4>MANDA</h4>
                  <p>la tua cartolina!<br>L’invio è gratuito</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4">
            <div class="box-container">
              <div class="graybox">
                <div class="icon">
                  <img src="/img/sostieni.svg" alt="Wikimedia Italia">
                </div>
                <div class="description">
                  <h4>SOSTIENI</h4>
                  <p>le nostre bellezze e la diffusione<br>della conoscenza libera</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="greenline"></div>
      </div>
    </div>
  </div>




  <!-- before cartoline -->
  <div id="cards-grid" class="cards container">
    <div class="row">
      <?php
      print_cards($cartoline);
      ?>
    </div>
    <div class="cards-info text-center">
      <p>Tutte le foto sono rilasciate in CC BY-SA</p>
    </div>
  </div>


  <!-- after cartoline -->

  <div id="why-donate">
    <div class="shadow"></div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h3>PERCHÉ SOSTENERE<BR>WIKIMEDIA ITALIA</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-lg-8">
            <p>Se anche tu credi che la conoscenza non<br>abbia limiti e che ci sia un enorme<br>potenziale da esprimere attraverso<br>l’utopia della scrittura infinita, sostienici<br>con una donazione.</p>
            <div class="donate-link">
              <a href="https://sostieni.wikimedia.it/dai_valore_alla_bellezza/" target="_blank" class="bg_red">INSIEME POSSIAMO FARE QUALCOSA DI IMPORTANTE</a>
            </div>
          </div>
          <div class="col-sm-12 col-md-8"></div>
        </div>
      </div>
  </div>

  <div id="do-you-know" class="text-center">
    <div class="inner">
      <div class="box-wrapper container">
        <div class="row">
          <div class="col-12 col-md-4">
            <div class="logo">
              <img src="/img/wlm-v.png" alt="">
            </div>
            <div class="box-container">
              <div class="graybox">
                <div class="triangle"></div>
                <div class="description">
                  <div class="title">
                    <h4>Conosci Wiki Loves<br>Monuments?</h4>
                  </div>
                  <div class="paragraph">
                    <p>Wiki Loves Monuments è l’edizione italiana del più grande concorso fotografico del mondo, un contest dedicato ai monumenti più belli del nostro meraviglioso patrimonio culturale. Le fotografie vengono pubblicate e condivise su Wikimedia Commons, per garantire la libera condivisione della cultura.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4 wmi-italia">
            <div class="logo">
              <img src="/img/wmi-logo.png" alt="">
            </div>
            <div class="box-container">
              <div class="graybox">
                <div class="triangle"></div>
                <div class="description">
                  <div class="title">
                    <h4>Conosci<br>Wikimedia Italia?</h4>
                  </div>
                  <div class="paragraph">
                    <p>Wikimedia Italia è il capitolo italiano di Wikimedia Foundation ed è l’associazione che in Italia sostiene Wikipedia, i progetti Wikimedia e OpenStreetMap. Promuove lo sviluppo e la diffusione di contenuti liberi in Italia, collaborando con volontari, istituzioni pubbliche e private, musei, biblioteche, archivi, scuole e università e organizzando dal 2012 Wiki Loves Monuments.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-4">
            <div class="logo sostienici">
              <img src="/img/sostienici.svg" alt="">
            </div>
            <div class="box-container">
              <div class="graybox">
                <div class="triangle"></div>
                <div class="description">
                  <div class="title">
                    <h4>Aiutaci anche tu:<br>sostienici!</h4>
                  </div>
                  <div class="paragraph">
                    <p>Aiutaci a portare i progetti Wikimedia nelle scuole, nei musei e nelle biblioteche. Insieme possiamo diffondere la cultura del software open source e delle licenze libere. Crediamo nella condivisione gratuita della conoscenza, perché nessuno possa trovare ostacoli per imparare qualcosa di nuovo o apprezzare la bellezza di un quadro, di una statua o di un paesaggio.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="greenline"></div>
      </div>
    </div>
  </div>

</div> <!-- ./#wrapper -->

<?php
  /*
  echo '<div id="debug_area">';
    echo 'Totale domande caricate: '.$questions_lenght;
    echo '<br/>';
    print_r($questions_array);
  echo '</div>';
  */

  include 'theme/footer.php';
