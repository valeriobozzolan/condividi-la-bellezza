<?php

define( 'SMTP_HOSTNAME', 'smtp.host.example.com' );
define( 'SMTP_AUTH', true );
define( 'SMTP_USERNAME', 'test@wikimedia.it' );
define( 'SMTP_PASSWORD', 'INSERT HERE YOUR SECRET PASSWORD' );
define( 'SMTP_SECURE', 'tls' );
define( 'SMTP_PORT', 587 );
define( 'EMAIL_FROM_ADDR', 'test@wikimedia.it' );
define( 'EMAIL_FROM_NAME', 'Wikimedia Italia EXAMPLE' );
define( 'EMAIL_REPLYTO_ADDR', 'test@wikimedia.it' );
define( 'EMAIL_REPLYTO_NAME', MAIL_FROM_NAME );
