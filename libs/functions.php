<?php

function print_cards($cartoline){

  $marks = array();
  foreach ($cartoline as $key => $row)
  {
      $marks[$key] = $row['where'];

  }
  array_multisort($marks, SORT_ASC, $cartoline);


  foreach($cartoline as $cartolina_key => $cartolina) {
    echo "<div class='col-12 col-sm-12 col-md-6 col-lg-4'>\n";

    echo print_card($cartolina);
    //echo $i;
    //$i++;
    echo "</div>";
  }
}




function print_card($cartolina) {

  $cartolina_title_safe_attr = htmlspecialchars( $cartolina['title'] );
  $licence_link = '';

  if($cartolina['licence_type'] == 'CCBYSA30'){
    $licence_link = 'https://creativecommons.org/licenses/by-sa/3.0/it/';
  }
  else if ($cartolina['licence_type'] == 'CCBYSA40'){
    $licence_link = 'https://creativecommons.org/licenses/by-sa/4.0/deed.it';
  }

  $content = '';

    $content .= "<div class='card_wrapper'>\n";
      $content .= "\t<div class='card_button' data-toggle='modal' data-target='#cardmodal_".$cartolina['id']."' data-cardtitle=\"$cartolina_title_safe_attr\" onclick=\"_paq.push(";
      // the title needs to be JSON encoded and then escaped for an HTML attribute
      $content .= htmlspecialchars( json_encode( [
          "trackEvent",
          "Interazione",
          "Click modal cartolina",
          $cartolina['title'],
        ] ) ) . ");\" >\n";

        $content .= "\t\t<div class='card_img'>\n";
        $content .= '<img class="card_teaserimg" alt="'.$cartolina['title'].'" src="'.$cartolina['img_teaser'].'" />';
        $content .= "\t\t</div>\n <!-- ./card_img -->";

        $content .= "\t\t<div class='card_title'>\n";
          $content .= "\t\t\t<div class='preview_title'>";
          $content .= "\t\t\t\t<h3>".$cartolina['title']."</h3>\n";
          $content .= "\t\t\t\t<p>".$cartolina['where']."<br/>di ".$cartolina['auth_name']."</p>\n";
          $content .= "\t\t\t</div>";
        $content .= "\t\t</div><!-- ./card_title -->\n";

      $content .= "\t</div><!-- ./card_button -->\n";
    $content .= "</div> <!-- ./card_wrapper -->\n";




    $content .= '
    <!-- Modal -->
    <div class="modal fade" id="cardmodal_'.$cartolina['id'].'" tabindex="-1" role="dialog" aria-labelledby="ModalLabel'.$cartolina['id'].'" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="ModalLabel'.$cartolina['id'].'">Regala una cartolina:<br/><strong>'.$cartolina['title'].'</strong> </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-12 col-lg-7">

                <img alt="'.$cartolina['title'].'" src="'.$cartolina['img_teaser'].'" />

                <div class="card_author">';

		// this is sometime undefined
                if ( !empty( $cartolina['auth_link'] ) ) {
                  $content .= '<p>'.$cartolina['where'].' - Fotografia di <a href="'.$cartolina['auth_link'].'" target="_blank">'.$cartolina['auth_name'].'</a></p>';
                }
                else{
                  $content .= '<p>'.$cartolina['where'].' - Fotografia di '.$cartolina['auth_name'].' </p>';
                }
                $content .= '
                </div>


                <div class="card_full_link">
                  <a href="'.$cartolina['img_full'].'" target="_blank">Visualizza il file originale   <i class="fas fa-external-link-alt"></i></a>
                </div>

                <div class="licence">
                  <p><a rel="license" href="'.$licence_link .'"><img class="licence_icon" src="/img/by-sa.svg" alt="CC BY-SA license logo" /> '.$cartolina['licence'].'</a></p>
                </div>

              </div>
              <div class="col-12 col-lg-5">

                <form class="current_form form_'.$cartolina['id'].'">
                  <div class="form_block">
                    <h3>Destinatario</h3>
                    <div class="form-group">
                      <label for="email_destinatario'.$cartolina['id'].'">Email desinatario</label>
                      <input name="email_destinatario" type="email" class="form-control" id="email_destinatario'.$cartolina['id'].'" placeholder="Inserisci l\'indirizzo email del destinatario">
                    </div>

                    <div class="form-group">
                      <label for="nome_destinatario'.$cartolina['id'].'">Nome destinatario: verrà usato per intestare l\'email</label>
                      <input name="nome_destinatario" type="text" class="form-control" id="nome_destinatario'.$cartolina['id'].'"  placeholder="Inserisci il nome del destinatario">
                    </div>
                  </div>

                  <div class="form_block">
                    <h3>Mittente</h3>
                    <div class="form-group">
                      <label for="nome_mittente'.$cartolina['id'].'">Nome mittente: verrà mostrato nel messaggio inviato</label>
                      <input name="nome_mittente" type="text" class="form-control" id="nome_mittente'.$cartolina['id'].'"  placeholder="Inserisci il tuo nome">
                    </div>
                    <div class="form-group">
                      <label for="messaggio_email'.$cartolina['id'].'">Lascia un messaggio al tuo destinatario</label>
                      <textarea name="messaggio_email" class="form-control" id="messaggio_email'.$cartolina['id'].'" ></textarea>
                    </div>
                  </div>

                  <div class="form_block">
                    <div class="form-check">
                      <input name="privacy" class="form-check-input" type="checkbox" value="" id="privacy_checkbox'.$cartolina['id'].'">
                      <label class="form-check-label" for="privacy_checkbox'.$cartolina['id'].'">
                        Accetto le condizioni della privacy policy
                      </label>
                    </div>
                  </div>

                  <input name="card_id" type="hidden" class="form-control" value="'.$cartolina['id'].'" >

                  <div class="btn btn-primary sendform" onclick="_paq.push([\'trackEvent\',\'Interazione\',\'Invio cartolina\',\''.$cartolina['title'].'\']);">Invia</div>
                  <!-- <input id="sendform_btn" type="submit" class="btn btn-primary sendform" disabled value="Invia"> -->
                  <!-- <button id="sendform_btn" type="submit" class="btn btn-primary sendform" disabled>Invia</button> -->

                </form>

                <div class="ajaxresult"></div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';


  /*
  'id' => 0,
  'title' => 'Anfiteatro Campano',
  'subject' => 'Anfiteatro Campano dell\'Antica Capua',
  'auth_name' => 'Nicola D\'Orta',
  'where' => 'Santa Maria Capua Vetere ',
  'award' => 'Primo premio concorso nazionale Wiki Loves Monuments 2012',
  'licence' => 'CC BY-SA 3.0, attraverso Wikimedia Commons',
  'img_teaser' => './img/800px-Anfiteatro_Campano_-_003.jpg',
  'img_full' => 'https://commons.wikimedia.org/wiki/File:Anfiteatro_Campano_-_003.jpg',
  */



  return $content;
}


?>
